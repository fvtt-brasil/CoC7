# FoundryVTT Pathfinder 2e Brazilian Portuguese

## Português

Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do Foundry VTT. Selecionar essa opção traduzirá a ficha de personagem do sistema CoC7. Esse módulo traduz somente aspectos relacionados ao sistema de [Call of Cthulhu 7th edition (Unofficial)](https://foundryvtt.com/packages/CoC7). Esse módulo não traduz outras partes do software Foundry VTT, como a interface principal. Para isso, confira o módulo [Brazilian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).


### Instalação por Manifesto
Na opção Add-On Modules clique em Install Module e coloque o seguinte link no campo Manifest URL

`https://gitlab.com/fvtt-brasil/CoC7/-/raw/master/coc7-ptbr/module.json`


## English
This module adds the language Português (Brasil) as an option to be selected in the Foundry VTT settings. Selecting this option will translate the character sheets of the CoC7 system. This module translates only aspects related to the [Call of Cthulhu 7th edition (Unofficial)](https://foundryvtt.com/packages/CoC7) system. This module doesn't translate other parts of the Foundry VTT software, like the main interface. For that, take a look at the [Brazilian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/) module.


### Manifest Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL

`https://gitlab.com/fvtt-brasil/CoC7/-/raw/master/coc7-ptbr/module.json`
